@extends('admin.layouts.app')
@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold float-left text-primary">Student card</h6>
            <button class="btn float-right btn-primary print">Print</button>
        </div>
        <div class="card-body">
            <div class="w-75 border p-2 m-auto print-area">
                <div class="row">
                    <div class="col-3">

                        <div style="width:200px; height:180px; border:2px solid #303030; background:#fff;">
                            <img src="{{ asset($student->student_profile_pic) }}" alt="" width="100%" height="100%" >
                        </div>

                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <span class="d-block">Student #:</span>
                                <span class="d-block h5">{{ $student->student_roll_no }}</span>
                            </div>
                        </div>

                    </div>

                    <div class="col-9 pl-4">
                        <p class="h1 text-center">The Jinnah School</p>

                        <div class="row">

                            <div class="col-sm-6">
                                <span class="d-block">Student Name:</span>
                                <span class="d-block h4">{{ $student->student_name }}</span>
                            </div>

                            <div class="col-sm-6">
                                <span class="d-block">Father Name:</span>
                                <span class="d-block h4">{{ $student->student_father_name }}</span>
                            </div>

                        </div>
                        
                        <div class="row">

                            <div class="col-sm-6">
                                <span class="d-block">Date of Birth:</span>
                                <Address class="d-block h5">{{ $student->dob }}</Address>
                            </div>

                            <div class="col-sm-6">
                                <span class="d-block">Class & Section:</span>
                                <Address class="d-block h5">{{ $student->class->class_title }} -- {{ $student->class->section_name  }}</Address>
                            </div>

                        </div>
                        

                        <div class="row">

                            <div class="col-sm-6">
                                <span class="d-block">Address:</span>
                                <Address class="d-block h5">{{ $student->student_address }}</Address>
                            </div>

                            <div class="col-sm-6">
                                <span class="d-block">Phone #:</span>
                                <Address class="d-block h5">{{ $student->student_guardian_phone_no }}</Address>
                            </div>

                        </div>

                    </div>

                </div>

            </div>
            
        </div>
    </div>

@endsection


@section('script')
    <script>
        $(document).ready(function(){
            $(".print").on('click',function(){
                $(".print-area").printThis();
            });
        });
    </script>
@endsection