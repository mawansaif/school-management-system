@extends('admin.layouts.app')
@section('content')

    @if(Session::has('message'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        {{ Session::get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
    </div>
    @endif

    
    @if(Session::has('error'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ Session::get('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
    </div>
    @endif


    @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
    
    @endif
    <!-- DataTales of Subjects -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary float-left">Class & Section List</h6>
        <button class="btn btn-primary float-right print">Print</button>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-sm" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>#</th>
                <th>Class</th>
                <th>Section</th>
                <th>Subjects </th>
                <th>Practical Subjects</th>
                <th>Seats</th>
                <th class="noprint">Action</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>#</th>
                <th>Class</th>
                <th>Section</th>
                <th>Subjects</th>
                <th>Practical Subjects</th>
                <th>Seats</th>
                <th class="noprint">Action</th>
              </tr>
            </tfoot>
            <tbody>
              @foreach($data['classes'] as $key => $class)
              <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $class->class_title }}</td>
                <td>{{ $class->section_name }}</td>
                <td>{{ $class->subjects->subjects }}</td>
                <td>{{ $class->subjects->practical_subjects }}</td>
                <td>{{ $class->seats }}</td>
                <td class="noprint">
                  <button class="btn btn-success btn-edit" data-id="{{ $class->id }}" data-class="{{ $class->class_title }}" data-section="{{ $class->section_name }}" data-subject="{{ $class->subjects->id }}" data-seat="{{ $class->seats }}" >
                    <i class="fas fa-edit"></i>
                  </button>
                  <button class="btn btn-danger delete-btn" data-id="{{ $class->id }}">
                    <i class="fas fa-trash"></i>
                  </button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>

        <!-- wqarning modal -->
        <div class="modal fade animate__animated animate__pulse" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="warningModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Warning</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Warning!</h4>
                        <p>If you delete this class then due to your this action student which are enroll in this class also remove and subjects teachers,class teachers.</p>
                        <hr>
                        <p class="mb-0">Whenever you need to, perform this action give notice to your seniors.</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-procced-to-delete">Procced</button>
                </div>
                </div>
            </div>
        </div>
        <!-- warning modal -->

        <!-- Delete Modal -->
        <div class="modal fade animate__animated animate__heartBeat" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <form action="delete_class_section" method="post">

                <div class="modal-body text-center">
                    <i class="fa fa-exclamation-triangle fa-8x text-danger" aria-hidden="true"></i>
                    <p class="h2 mt-2">Confirm to delete it?</p>
                    @csrf
                    <input type="hidden" name="class_id" id="class_id">
                </div>

                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary mr-auto" data-dismiss="modal">Close</button>
                  <button class="btn btn-danger">Procced</button>
                </div>

              </form>

            </div>
          </div>
        </div>
        <!-- Delete Modal -->


        <!-- Edit Modal -->
        <div class="modal fade animate__animated animate__backInDown" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Edit Class & Section</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="edit_class_section">
                    <div class="modal-body">

                        @csrf

                        <input type="hidden" name="class_section_id" id="class_section_id" class="form-control">

                        <label>Class</label>
                        <input type="text" name="class_title" id="class_title" class="form-control @error('class_title') is-invalid @enderror"">
                        @error('class_title')
                            <span class="invalid-feedback ml-4" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                        <label>Section</label>
                        <input type="text" name="section" id="section" class="form-control">

      
                        <label>Subjects</label>
                        <select name="subjects" id="subjects" class="form-control">
                        <option value="" disabled selected hidden>Choose a subjects</option>
                          @foreach( $data['subjects'] as $subject)
                            <option id="subject{{ $subject->id }}" value="{{ $subject->id }}">{{ $subject->subjects }} -- {{ $subject->practical_subjects }}</option>
                          @endforeach
                        </select>

                        <label>Seats</label>
                        <input type="text" name="seats" id="seats" class="form-control">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button  class="btn btn-primary">Save changes</button>
                    </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- Edit Modal -->

@endsection

@section('script')
    <script>
        $(document).ready(function(){

          // print
          $(".print").on('click',function(){
            $(".table").printThis({
                printContainer: true,
                header: 'Class & sections List',
            });

          });

          // warning model
          $(".delete-btn").on('click',function(){
              $("#warningModal").modal('show');
          });

          // delete model
          $(".btn-procced-to-delete").on('click',function(){
              $("#warningModal").modal('hide');
              $("#class_id").val($('.delete-btn').attr('data-id'));
              $("#deleteModal").modal('show');
          });

          // edit model
          $(".btn-edit").on('click',function(){
            
            $("#class_section_id").val($(this).attr('data-id'));
            $("#class_title").val($(this).attr('data-class'));
            $("#section").val($(this).attr('data-section'));
            $("#subject"+$(this).attr('data-subject')).attr('selected', true);
            $("#seats").val($(this).attr('data-seat'))
            $("#editModal").modal('show');
          });

        });
    </script>
@endsection
