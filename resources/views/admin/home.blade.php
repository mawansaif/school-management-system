@extends('admin.layouts.app')


@section('content')

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
</div>

<!-- Content Row -->
<div class="row">

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Students</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                            @isset($data)
                            {{$data['students']}}
                            @endisset
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-user-graduate fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Teachers</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                            @isset($data)
                            {{$data['teachers']}}
                            @endisset
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fa fa-chalkboard-teacher fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-info shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Classes</div>
                        <div class="row no-gutters align-items-center">
                            <div class="col-auto">
                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
                                    @isset($data)
                                    {{$data['classes']}}
                                    @endisset
                                </div>
                            </div>
                            <div class="col">
                                <div class="progress progress-sm mr-2">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 30%"
                                        aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Pending Requests Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Users</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                            @isset($data['users'])
                            {{ $data['users'] }}
                            @endisset
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-comments fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row notification">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h3 class="float-left">Late Fee Student List</h3>
                <button class="btn float-right btn-primary btn-print-late-fee">Print</button>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered late-fee-students table-sm" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Roll No</th>
                                <th>Class & Section</th>
                                <th>Fee</th>
                                <th>Fee of Month</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                            <th>#</th>
                                <th>Name</th>
                                <th>Roll No</th>
                                <th>Class & Section</th>
                                <th>Fee</th>
                                <th>Fee of Month</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($data['late_fee'] as $key => $lateFee)
                            <tr>
                                <th>{{ $key + 1 }}</th>
                                <th>{{ $lateFee->student_fees->students->student_name }}</th>
                                <th>#{{ $lateFee->student_fees->students->student_roll_no }}</th>
                                <th>{{ $lateFee->student_fees->students->class->class_title}} | {{ $lateFee->student_fees->students->class->section_name}}</th>
                                <th>Rs.{{ $lateFee->fee_amount }}</th>
                                <th>{{ \Carbon\Carbon::parse($lateFee->fee_of_month)->toFormattedDateString() }}</th>
                            </tr>
                            @endforeach
                        </tbody>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12 mt-4">
        <div class="card">
            <div class="card-header">
                <h3 class="float-left">Struck Off Student</h3>
                <button class="btn btn-primary btn-print-struck-off float-right">Print</button>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered struck-off-student table-sm" id="dataTable2" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Roll No</th>
                                <th>Class & Section</th>
                                <th>Phone Number</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                            <th>#</th>
                                <th>Name</th>
                                <th>Roll No</th>
                                <th>Class & Section</th>
                                <th>Phone Number</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($data['struck_off'] as $key => $struckOff)
                            <tr>
                                <th>{{ $key + 1 }}</th>
                                <th>{{ $struckOff->student_name }}</th>
                                <th>#{{ $struckOff->student_roll_no }}</th>
                                <th>{{ $struckOff->class->class_title}} | {{ $struckOff->class->section_name}}</th>
                                <th>{{ $struckOff->student_guardian_phone_no }}</th>
                            </tr>
                            @endforeach
                        </tbody>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

<script>
    $(document).ready(function(){
        $("#dataTable2").dataTable();

        $(".btn-print-late-fee").on('click',function(){
            $(".late-fee-students").printThis({});
        });

        $(".btn-print-struck-off").on('click',function(){
            $(".struck-off-student").printThis();
        });
    });
</script>

@endsection