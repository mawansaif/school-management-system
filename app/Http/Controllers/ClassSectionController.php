<?php

namespace App\Http\Controllers;

use App\Models\ClassSection;
use App\Models\Subject;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class ClassSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $subjects = Subject::orderBy('subjects','ASC')->get();
        $classes = ClassSection::with('subjects')
                                ->orderBy('class_title','ASC')
                                ->get();
        $data = [
            'subjects'  => $subjects,
            'classes'   => $classes,
            'class_sections' => ClassSection::orderBy('class_title','ASC')
                                ->get()
        ];
        return view('admin.class_sections.main',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $subjects = Subject::orderBy('subjects','ASC')->get();
        return view('admin.class_sections.create',compact('subjects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(),[
            'class_title'    => 'required',
            'section_name'   => 'required',
            'class_subjects' => 'required',
            'class_seats'    => 'required',
        ]);
        if($validator->fails())
        {
            return redirect()->to('add_class_sections')->withErrors($validator)->withInput();
        }
        elseif(
            ClassSection::where('class_title',$request->class_title)
                        ->where('section_name',$request->section_name)
                        ->first()
            )
        {
            $request->session()->flash('error', 'This Class with this section name already exsit');
            return redirect()->to('add_class_sections')->withInput();
        }
        else
        {
            $data = new ClassSection;
            $data->class_title = $request->class_title;
            $data->section_name = $request->section_name;
            $data->subjects_id = $request->class_subjects;
            $data->seats = $request->class_seats;
            $check = $data->save();
            if($check)
            {
                $request->session()->flash('message', 'Class and section save successfuly');
                return redirect()->to('add_class_sections');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ClassSection  $classSection
     * @return \Illuminate\Http\Response
     */
    public function show(ClassSection $classSection)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ClassSection  $classSection
     * @return \Illuminate\Http\Response
     */
    public function edit(ClassSection $classSection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ClassSection  $classSection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClassSection $classSection)
    {
        //
        $validator = Validator::make($request->all(),[
            'class_title'    => 'required',
            'section'   => 'required',
            'subjects' => 'required',
            'seats'    => 'required|integer|min:20',
        ]);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator);
        }
        elseif(
            ClassSection::where('id','!=',$request->class_section_id)
                        ->where('class_title',$request->class_title)
                        ->where('section_name', $request->section)
                        ->first()
            )
        {
            $request->session()->flash('error', 'The selected class already have section with this name');
            return redirect()->back();
        } 
        else
        {
            $check = ClassSection::where('id',$request->class_section_id)
                                ->update([
                                    'class_title' => $request->class_title,
                                    'section_name'=> $request->section,
                                    'subjects_id' => $request->subjects,
                                    'seats'       => $request->seats,
                                ]);
            if($check)
            {
                $request->session()->flash('message', 'Class and section save successfuly');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ClassSection  $classSection
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,ClassSection $classSection)
    {
        //
        $delete  = ClassSection::where('id',$request->class_id)->delete();
        if($delete)
        {
            $request->session()->flash('message', 'Class and section removed successfuly');
            return redirect()->back();
        }
        else{
            
        }
    }
}
